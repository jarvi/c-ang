import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LazyRoutingModule } from './lazy-routing.module';
import { LazyComponent } from './lazy.component';
import { lazyHeaderComp } from '../../components/lazy-header-comp/header.component';

@NgModule({
  declarations: [LazyComponent, lazyHeaderComp],
  imports: [CommonModule, LazyRoutingModule],
})
export class LazyModule {}
