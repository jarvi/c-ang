import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LazyComponent } from './lazy.component';
import { HeaderComponent } from '../../components/header/header.component';
//import { DetailComponent } from './detail/detail.component';
//const routes: Routes = [{ path: '', component: LazyComponent, }];
const routes: Routes = [
  { path: '', component: LazyComponent },
  //{ path: '/:title', component: DetailComponent },
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LazyRoutingModule {}
