import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root',
})
export class PersonalInformationDataService {
  private dataFormPesonal = new BehaviorSubject<any>({});
  public objectOserver = this.dataFormPesonal.asObservable();
  constructor() {}
  public changeDataFormPesonal(currentData: any): void {
    this.dataFormPesonal.next(currentData);
  }
}
