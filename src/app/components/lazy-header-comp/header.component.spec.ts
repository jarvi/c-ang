import { ComponentFixture, TestBed } from '@angular/core/testing';

import { lazyHeaderComp } from './header.component';

describe('HeaderComponent', () => {
  let component: lazyHeaderComp;
  let fixture: ComponentFixture<lazyHeaderComp>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [lazyHeaderComp],
    }).compileComponents();

    fixture = TestBed.createComponent(lazyHeaderComp);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
