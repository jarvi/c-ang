import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { PersonalInformationDataService } from 'src/app/provaiders/personal-information-data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css'],
})
export class FooterComponent {
  public x: any = 'ee';
  public nombre = '';
  dataform: any = {};

  @Input() name: String = '';

  ngOnInit(): void {
    console.log('cargo en el dom, ');

    this._route.params.subscribe((params: Params) => {
      console.log(params['id']);
      this.nombre = params['id'];
    });

    this.personalInformationData.objectOserver.subscribe((currentData) => {
      this.dataform = JSON.stringify(currentData) ;
      console.log(currentData);
    });
  }

  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private personalInformationData: PersonalInformationDataService
  ) {
    console.log('en el costructor');
    this.x = 155;
    console.log(this.x);
  }

  redirection() {
    this._router.navigate(['/page', 'jarvis', 'rangel']);
  }
}
